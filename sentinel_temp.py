import sentinelsat
import netCDF4
from matplotlib import pyplot as plt
import pandas as pd
import numpy
import sys
import os


# Using "S3A_SL_2_LST____20191022T203224_20191022T203524_20191022T224831_0179_050_328_0720_LN2_O_NR_003.SEN3" dataset from Open Access Hub; this is also the name of a folder searched for below


dirs = [x[0] for x in os.walk("./")]    # Finds a folder in the same directory as the program (make sure that the only folders in this directory are the relevant ones)


for i in range(1,len(dirs)):

    dir = dirs[i]
    dir = dir.replace("./", "")     # Gets the name of the folder
    
    loc=dir+'/geodetic_in.nc'       # Finds .nc files relevant for my example inside the folder with the data
    grided_file=dir+'/LST_in.nc'    #

    
    ncloc = netCDF4.Dataset(loc)                # Creates files readable by python
    grided = netCDF4.Dataset(grided_file)       #


    x=ncloc['latitude_in'][:]            #
    y=ncloc['longitude_in'][:]           # Uses pandas for formating
    grided_temp=grided['LST'][:]         #


    lat = x.flatten()                       #
    lon = y.flatten()                       # Creates a single list from pandas format
    temperature = grided_temp.flatten()     #



    coo = list(zip(lat,lon,temperature))       # Creates a list of tuples (there has to b the same number of each informatin ziped)
    filtered_coo = list(filter(lambda x: x[2] is not numpy.ma.masked, coo))     # Removes all the empty temperature fields (probably sea or lake; this surface temperature data)

    print(filtered_coo)     # Prints parsed data
    
    with open('temp_data.txt', 'w') as file:        # Saves the data in a file in your directory
        file.write(str(filtered_coo))
    
    
